package com.example.StoreBillingSystem.service;

import com.example.StoreBillingSystem.entity.ProductList;

import java.util.List;

public interface ProductService {
    ProductList insert(ProductList productList);
    List<ProductList> getAllProducts();
    boolean isProductExist(Long id);
    ProductList storeProduct(ProductList productList);
    Double calculateFinalPrice();
    Double calculateTotalTax();
    ProductList getProductById(Long id);
    void deleteProductById(Long id);

}
