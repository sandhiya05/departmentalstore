package com.example.StoreBillingSystem.service;

import com.example.StoreBillingSystem.repository.ProductRepository;
import com.example.StoreBillingSystem.entity.ProductList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImplementation implements ProductService {
        @Autowired
        private ProductRepository productRepository;

        public ProductServiceImplementation(ProductRepository productRepository) {
            this.productRepository = productRepository;
        }

        @Override
        public ProductList insert(ProductList productList){
            return productRepository.save(productList);
        }

        @Override
        public List<ProductList> getAllProducts() {
            return productRepository.findAll();
        }

        @Override
        public ProductList getProductById(Long id) {
            return productRepository.findById(id).get();
        }

        @Override
        public boolean isProductExist(Long id){
            return productRepository.existsById(id);
        }

        @Override
        public ProductList storeProduct(ProductList productList) {
            return productRepository.save(productList);
        }


        @Override
        public void deleteProductById(Long id) {
            productRepository.deleteById(id);
        }

    @Override
    public Double calculateFinalPrice(){

        Double total_tax = 0.0;
        Double total_price = 0.0;
        Double taxPerProduct = 0.0;
        Double unitPrice = 0.0;


        List<ProductList> productList = getAllProducts();
        productList.forEach(product-> {

            if (product.getProductType().contains("imported")) {
                if (product.getProductType().contains("book") || product.getProductType().contains("food") || product.getProductType().contains("medicine")) {
                    taxPerProduct.set(product.getPricePerProduct() * 0.05);
                    unitPrice.set((product.getPricePerProduct() + taxPerProduct.get()) * product.getQuantity());
                    total_tax.updateAndGet(v -> v + (taxPerProduct.get() * product.getQuantity()));
                    total_price.updateAndGet(v -> v + product.getPricePerProduct());
                }
                else {
                    taxPerProduct.set(product.getPricePerProduct() * 0.15);
                    unitPrice.set((product.getPricePerProduct() + taxPerProduct.get()) * product.getQuantity());
                    total_tax.updateAndGet(v -> v + (taxPerProduct.get() * product.getQuantity()));
                    total_price.updateAndGet(v -> v + product.getPricePerProduct());
                }
            }
            else {
                if (product.getProductType().contains("book") || product.getProductType().contains("food") || product.getProductType().contains("medicine")) {
                    unitPrice.set((product.getPricePerProduct() + taxPerProduct.get()) * product.getQuantity());
                    total_price.updateAndGet(v -> v + product.getPricePerProduct());
                }
                else {
                    taxPerProduct.set(product.getPricePerProduct() * 0.10);
                    unitPrice.set((product.getPricePerProduct() + taxPerProduct.get()) * product.getQuantity());
                    total_tax.updateAndGet(v -> v + (taxPerProduct.get() * product.getQuantity()));
                    total_price.updateAndGet(v -> v + product.getPricePerProduct());
                }
            }

        });

        return total_price.get();

    }

    @Override
    public Double calculateTotalTax(){

        Double total_tax = 0.0;
        Double total_price = 0.0;
        Double taxPerProduct = 0.0;
        Double pricePerProduct = 0.0;

        List<ProductList> productList = getAllProducts();
        productList.forEach(product-> {


            if (product.getProductType().contains("imported")) {
                if (product.getProductType().contains("book") || product.getProductType().contains("food") || product.getProductType().contains("medicine")) {
                    taxPerProduct.set(product.getPricePerProduct() * 0.05);
                    product.set((product.getPricePerProduct() + taxPerProduct.get()) * product.getQuantity());
                    total_tax.updateAndGet(v -> v + (taxPerProduct.get() * product.getQuantity()));
                    total_price.updateAndGet(v -> v + product.getPricePerProduct());
                }
                else {
                    taxPerProduct.set(product.getPricePerProduct() * 0.15);
                    product.set((product.getPricePerProduct() + taxPerProduct.get()) * product.getQuantity());
                    total_tax.updateAndGet(v -> v + (taxPerProduct.get() * product.getQuantity()));
                    total_price.updateAndGet(v -> v + product.getPricePerProduct());
                }
            }
            else {
                if (product.getProductType().contains("book") || product.getProductType().contains("food") || product.getProductType().contains("medicine")) {
                    product.set((product.getPricePerProduct() + taxPerProduct.get()) * product.getQuantity());
                    total_price.updateAndGet(v -> v + product.getPricePerProduct());
                }
                else {
                    taxPerProduct.set(product.getPricePerProduct() * 0.10);
                    product.set((product.getPricePerProduct() + taxPerProduct.get()) * product.getQuantity());
                    total_tax.updateAndGet(v -> v + (taxPerProduct.get() * product.getQuantity()));
                    total_price.updateAndGet(v -> v + product.getPricePerProduct());
                }
            }

        });

        return total_tax.get();

    }


}
