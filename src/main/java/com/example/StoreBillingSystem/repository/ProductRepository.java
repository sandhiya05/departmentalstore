package com.example.StoreBillingSystem.repository;

import com.example.StoreBillingSystem.entity.ProductList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductList, Long> {
        //all crud databse methods
}
