package com.example.StoreBillingSystem.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "products")
public class ProductList {
    @Id
    private Long id;

    @Column(name = "product_type")
    private String productType;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "price_per_product")
    private Double PricePerProduct;


    public Double getTotalPrice() {
        Double total_tax = 0.0;
        Double total_price = 0.0;
        Double taxPerProduct = 0.0;

        if (productType.contains("imported")) {
            if (productType.contains("book") || productType.contains("food") || productType.contains("medicine")) {
                taxPerProduct = PricePerProduct * 0.05;
                PricePerProduct = (PricePerProduct + taxPerProduct) * quantity;
                total_tax += (taxPerProduct * quantity);
                total_price += PricePerProduct;
            } else {
                taxPerProduct = PricePerProduct * 0.15;
                PricePerProduct = (PricePerProduct + taxPerProduct) * quantity;
                total_tax += (taxPerProduct * quantity);
                total_price += PricePerProduct;
            }
        } else {
            if (productType.contains("book") || productType.contains("food") || productType.contains("medicine")) {
                PricePerProduct = (PricePerProduct * quantity);
                total_price += PricePerProduct;
            } else {
                taxPerProduct = PricePerProduct * 0.10;
                PricePerProduct = (PricePerProduct + taxPerProduct) * quantity;
                total_tax += (taxPerProduct * quantity);
                total_price += PricePerProduct;
            }
        }
        return total_price;
    }


    public Double getTotalTax() {
        Double total_tax = 0.0;
        Double total_price = 0.0;
        Double taxPerProduct = 0.0;

        if (productType.contains("imported")) {
            if (productType.contains("book") || productType.contains("food") || productType.contains("medicine")) {
                taxPerProduct = PricePerProduct * 0.05;
                PricePerProduct = (PricePerProduct + taxPerProduct) * quantity;
                total_tax += (taxPerProduct * quantity);
                total_price += (PricePerProduct + total_tax);
            } else {
                taxPerProduct = PricePerProduct * 0.15;
                PricePerProduct = (PricePerProduct + taxPerProduct) * quantity;
                total_tax += (taxPerProduct * quantity);
                total_price += (PricePerProduct + total_tax);
            }
        } else {
            if (productType.contains("book") || productType.contains("food") || productType.contains("medicine")) {
                PricePerProduct = (PricePerProduct * quantity);
                total_price += (PricePerProduct);
            } else {
                taxPerProduct = PricePerProduct * 0.10;
                PricePerProduct = (PricePerProduct + taxPerProduct) * quantity;
                total_tax += (taxPerProduct * quantity);
                total_price += (PricePerProduct + total_tax);
            }
        }
        return total_tax;
    }
}
