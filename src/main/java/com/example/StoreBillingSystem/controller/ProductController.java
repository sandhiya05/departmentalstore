package com.example.StoreBillingSystem.controller;

import com.example.StoreBillingSystem.entity.ProductList;
import com.example.StoreBillingSystem.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    public ProductController(ProductService productService) {
        super();
        this.productService = productService;
    }

    @GetMapping("/products")
    public List<ProductList> ListOfProducts() {
        return productService.getAllProducts();
    }
    @PostMapping("/products/new")
    public ProductList createProductList(@RequestBody ProductList productList) {
        productService.insert(productList);
        return productService.storeProduct(productList);
    }

    @GetMapping("/TotalPrice")
    public Double CalcTotalPrice(){
        return productService.calculateFinalPrice();
    }

    @GetMapping("/TotalTax")
    public Double CalcTotalTax(){
        return productService.calculateTotalTax();
    }

    @DeleteMapping("/products/{id}")
    public String deleteProduct(@PathVariable Long id) {
        if(productService.isProductExist(id)) {
            productService.deleteProductById(id);
            return "Product Deleted successfully!";
        }
        else {
            return "Product does not exist in the bill!";
        }
    }

}
